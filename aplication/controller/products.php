<?php
/**
 * 
 */
class ProductsController {
	public static function saveProduct($array){
		$class = $array["productType"];
		$product = new $class($array["sku"], $array["name"], $array["price"], $array[$array["productType"] . "SpecificInfo"]);

		echo $product->save();
	}

	public static function getProducts(){
		return Product::getAll();
	}

	public static function removeProducts($array){
		foreach ($array as $row) {
			$info = explode("|", $row);
			$product = new $info[1]($info[0]);
			echo $product->delete();
		}
	}

}

?>
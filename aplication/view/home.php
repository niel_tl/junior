<?php
	include('html/head.html');
?>


<div class="d-sm-flex justify-content-between align-items-center mt-3 mx-3">
		<h1 class="display-5">Product List</h1>
		<div>
			<a class="btn btn-primary" href="addproduct">ADD</a>
			<button class="btn btn-danger" id="btn_delete">MASS DELETE</button>
		</div>
</div>
<hr>
<div class="container-fluid overflow-hidden">
	<div class="row gx-3 gy-3">
		<br>
		<?php
			$products = ProductsController::getProducts();

			foreach($products as $product){
		?>
		<div class="col-lg-3 col-md-4 col-sm-6 col-12">
			<div class="border border-2 border-secondary rounded">
				<div class="form-check"><input type="checkbox" id="<?php echo $product->getSku() . '|' . $product->getClass() ?>" class="delete-checkbox" style="margin-top: 5px; width: 20px; height: 20px;"></div>
				<div class="text-center">
					<div><?php echo $product->getSku() ?></div>
					<div><?php echo $product->getName() ?></div>
					<div>$ <?php echo $product->getPrice() ?></div>
					<div><?php echo $product->displaySpecificInfo() ?></div>
					<br>
				</div>
			</div>
		</div>
		<?php
			}
		?>
	</div>
</div>

<?php
	include('html/footer.html');
?>

<script type="text/javascript" src="/js/control/remove_product.js"></script>

<?php
	include('html/foot.html');
?>
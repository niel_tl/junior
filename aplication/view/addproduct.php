<?php
	include('html/head.html');
?>

<div class="d-sm-flex justify-content-between align-items-center mt-3 mx-3">
		<h1 class="display-5">Product Add</h1>
		<div>
			<button id="btn_save" class="btn btn-success">Save</button>
			<a class="btn btn-secondary" href="/">Cancel</a>
		</div>
</div>
<hr>
<div class="container overflow-hidden">
	<form class="needs-validation" id="product_form" method="post" novalidate>
		<div class="form-group row m-3">
		    <label for="sku" class="col-sm-2 col-form-label">SKU:</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="sku" id="sku" maxlength="20" required>
		    	<div class="invalid-feedback">
		    		Please provide the SKU.
		    	</div>
		    </div>
		</div>
		<div class="form-group row m-3">
		    <label for="name" class="col-sm-2 col-form-label">Name:</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="name" id="name" required>
			    <div class="invalid-feedback">
			    	Please provide the name.
			    </div>
		    </div>
		</div>
		<div class="form-group row m-3">
		    <label for="price" class="col-sm-2 col-form-label">Price ($):</label>
		    <div class="col-sm-10">
		    	<input type="text" class="form-control" name="price" id="price" required>
			    <div class="invalid-feedback">
			    	Please provide the price in dollars.
			    </div>
		    </div>
		</div>
		<div class="form-group row m-3">
	    	<label for="productType" class="col-sm-2 col-form-label">Product type:</label>
			<div class="col-sm-10">
				<select class="form-control custom-select" name="productType" id="productType" required>
      				<option value="">Select the product type</option>
					<option value="Dvd">DVD</option>
					<option value="Book">Book</option>
					<option value="Furniture">Furniture</option>
				</select>
			    <div class="invalid-feedback">
			    	Please select a product type.
			    </div>
			</div>
		</div>
		<div class="type_section" id="dvd_section" style="display: none">
			<div class="form-group row m-3">
			    <label for="size" class="col-sm-2 col-form-label">Size (MB):</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" name="DvdSpecificInfo" id="size" min="0">
				    <div class="invalid-feedback">
				    	Please provide the size in MB.
				    </div>
			    </div>
			</div>
		</div>
		<div class="type_section" id="book_section" style="display: none">
			<div class="form-group row m-3">
			    <label for="weight" class="col-sm-2 col-form-label">Weight (Kg):</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" name="BookSpecificInfo" id="weight" min="0">
				    <div class="invalid-feedback">
				    	Please provide the weight in Kg.
				    </div>
			    </div>
			</div>
		</div>
		<div class="type_section" id="furniture_section" style="display: none">
			<div class="form-group row m-3">
			    <label for="height" class="col-sm-2 col-form-label">Height:</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" name="height" id="height" min="0">
				    <div class="invalid-feedback">
				    	Please provide the height.
				    </div>
			    </div>
			</div>
			<div class="form-group row m-3">
			    <label for="width" class="col-sm-2 col-form-label">Width:</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" name="width" id="width" min="0">
				    <div class="invalid-feedback">
				    	Please provide the width.
				    </div>
			    </div>
			</div>
			<div class="form-group row m-3">
			    <label for="length" class="col-sm-2 col-form-label">Length:</label>
			    <div class="col-sm-10">
			    	<input type="text" class="form-control" name="length" id="length" min="0">
				    <div class="invalid-feedback">
				    	Please provide the length.
				    </div>
			    </div>
			</div>
		</div>
	</form>
</div>

<?php
	include('html/footer.html');
?>

<script type="text/javascript" src="/js/control/add_product.js"></script>
<script type="text/javascript" src="/js/jquery-inputformat.min.js"></script>

<?php
	include('html/foot.html');
?>
<?php
/**
 * 
 */
class Book extends Product {
	private $weight;

	function __construct($sku = null, $name = null, $price = null, $weight = null)
	{
		parent::__construct($sku, $name, $price);
		$this->class = "Book";
		$this->weight = $weight;
	}

	public function getSpecificInfo(){
		return $this->weight;
	}

	public function setSpecificInfo($weight){
		$this->weight = $weight;
	}

	public function displaySpecificInfo(){
		return $this->weight . " Kg";
	}

}
?>
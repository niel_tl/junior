<?php
/**
 * 
 */
class Dvd extends Product {
	private $weight;

	function __construct($sku = null, $name = null, $price = null, $size = null)
	{
		parent::__construct($sku, $name, $price);
		$this->class = "Dvd";
		$this->size = $size;
	}

	public function getSpecificInfo(){
		return $this->size;
	}

	public function setSpecificInfo($size){
		$this->size = $size;
	}

	public function displaySpecificInfo(){
		return $this->size . " MB";
	}

}
?>
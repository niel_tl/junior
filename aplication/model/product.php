<?php
/**
 * 
 */
abstract class Product{
	protected $sku;
	protected $name;
	protected $price;
	protected $class;
	private $table = "tb_product";

	function __construct($sku = null, $name = null, $price = null){
		$this->sku = $sku;
		$this->name = $name;
		$this->price = $price;
	}

	public function getSku(){
		return $this->sku;
	}

	public function setSku($sku){
		$this->sku = $sku;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getPrice(){
		return $this->price;
	}

	public function setPrice($price){
		$this->price = $price;
	}

	public function getClass(){
		return $this->class;
	}

	public abstract function getSpecificInfo();

	public abstract function setSpecificInfo($specificInfo);

	public abstract function displaySpecificInfo();

	public function save(){
		if(!$this->isSkuTaken()){
			$mysqli = Database::getInstance()->getConnection();

			$response;

			$stmt = $mysqli->prepare("INSERT INTO tb_products (sku, name, price, class, specificInfo) VALUES (?, ?, ?, ?, ?)");
			$stmt->bind_param("ssdss", $this->sku, $this->name, $this->price, $this->class, $this->getSpecificInfo());
			$stmt->execute();
			$response = $stmt->affected_rows;
			$stmt->close();

			return $response;
		}

		return "Sku not unique";
	}

	public function isSkuTaken(){
		$mysqli = Database::getInstance()->getConnection();

		$response;

		$stmt = $mysqli->prepare("SELECT * FROM tb_products WHERE sku = ?");
		$stmt->bind_param("s", $this->sku);
		$stmt->execute();
		$result = $stmt->get_result();
		$response = $result->num_rows;
		$stmt->close();

		return $response;
	}

	public function delete(){
		$mysqli = Database::getInstance()->getConnection();

		$response;

		$stmt = $mysqli->prepare("DELETE FROM tb_products WHERE sku = ?");
		$stmt->bind_param("s", $this->sku);
		$stmt->execute();
		$response = $stmt->affected_rows;
		$stmt->close();

		return $response;
	}

	public static function getAll(){
		$mysqli = Database::getInstance()->getConnection();
		$products = array();

		$sql = "SELECT * FROM tb_products";

		if ($result = $mysqli->query($sql)) {
	        while($line = $result->fetch_object()){
	        	$class = $line->class;
	        	$product = new $class($line->sku, $line->name, $line->price);
	        	$product->setSpecificInfo($line->specificInfo);
	        	array_push($products, $product);
	        }

	        return $products;
    	}
	}
}

?>
<?php
/**
 * 
 */
class Furniture extends Product {
	private $height;
	private $width;
	private $length;

	function __construct($sku = null, $name = null, $price = null, $dimensions = null)
	{
		parent::__construct($sku, $name, $price);
		$this->class = "Furniture";
		$dimensions = explode("x", $dimensions);
		if (count($dimensions) == 3) {
			$this->height = $dimensions[0];
			$this->width = $dimensions[1];
			$this->length = $dimensions[2];
		}
	}

	public function getSpecificInfo(){
		return $this->height . "x" . $this->width . "x" . $this->length;
	}

	public function setSpecificInfo($string){
		$dimensions = explode("x", $string);

		if (count($dimensions) == 3) {
			$this->height = $dimensions[0];
			$this->width = $dimensions[1];
			$this->length = $dimensions[2];
		}
	}

	public function displaySpecificInfo(){
		return $this->getSpecificInfo();
	}

}
?>
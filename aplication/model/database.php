<?php

class Database {
  private static $instance = null;
  private $conn;
  
  private function __construct(){
    $this->conn = new mysqli("localhost","id18419492_daniel","Scandiweb12@","id18419492_db_daniel");

    if($this->conn->connect_error) {
      exit('Error connecting to database'); //Should be a message a typical user could understand in production
    }
  }
 
  public static function getInstance(){
    if (self::$instance == null)
    {
      self::$instance = new Database();
    }
 
    return self::$instance;
  }

  public function getConnection(){
  	return $this->conn;
  }
}
?>
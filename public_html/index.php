<?php

function autoloadFunction($class)
{
    require_once("../aplication/model/" . strtolower($class) . ".php");
}


spl_autoload_register("autoloadFunction");

$request_uri = $_SERVER['REQUEST_URI'];

switch ($request_uri) {
    // Home page
    case '/':
        require_once '../aplication/controller/products.php';
        require_once '../aplication/view/home.php';
        break;
    // Add product page
    case '/addproduct':
        require_once '../aplication/view/addproduct.php';
        break;
    // Save product
    case '/saveproduct':  
        require_once '../aplication/controller/products.php';
        ProductsController::saveProduct($_POST);
        break;
    // Remove product
    case '/removeproduct':  
        require_once '../aplication/controller/products.php';
        ProductsController::removeProducts($_POST["products"]);
        break;
    // Everything else
    default:
        header('HTTP/1.0 404 Not Found');
        break;
}
?>
(function($){ 
	$(function() {
	    $("#btn_delete").click(function() {
	    	var checkedProducts = [];
			$(".delete-checkbox:checked").each(function(index, obj){
				checkedProducts[index] = $(obj).attr("id");
			});
			
			if (checkedProducts.length > 0) {
				$.post( 'removeproduct', {products : checkedProducts}, function(data) {
					if(data){
						location.reload();
					}
				});
			}
	    });
	});
})(jQuery);
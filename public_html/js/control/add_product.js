(function($){ 
	$(function() {
	    $("#product_form").submit(function( event ) {
			if (this.checkValidity() === true) {
				results = $(this).serializeArray();
				results.push({name: "FurnitureSpecificInfo", value: $("#height").val() + "x" + $("#width").val() + "x" + $("#length").val()});

				console.log(results);

				$.post( 'saveproduct', $.param(results), function(data) {
					if(data == "Sku not unique"){
						skuErrorMessage();
					} else if (data){
						window.location = "/";
					} else {
						alert("An unexpected error has occured.");
					}
				});
			}
			$(this).addClass('was-validated');
			$(".form-control:invalid").addClass("is-invalid");

			event.preventDefault();
			event.stopPropagation();
	    });

	    function skuErrorMessage(){
	    	skuField = $("#sku");
	    	skuField.addClass("is-invalid");
	    	skuField.siblings().text("This SKU is already registered.");
	    	$(".form-control:not(#sku)").addClass("is-valid");
	        $("#product_form").removeClass('was-validated');
	    }

	    $(".form-control").on("input", function(){
	        $("#product_form").removeClass('was-validated');
	    	$(this).removeClass("is-invalid");
	    });

		$('#price').inputNumber({
			thousandSep: '',
			decimalSep: '.',
			allowDecimals: true,
			allowNegative: false,
			allowLeadingZero: true,
			maxDecimalDigits: 2,
			numericOnly: false
		});

		$('.type_section .form-control').inputNumber({
			thousandSep: '',
			decimalSep: '.',
			allowDecimals: true,
			allowNegative: false,
			allowLeadingZero: true,
			maxDecimalDigits: 3,
			numericOnly: false
		});

		$("#btn_save").click(function() {
			$("#product_form").submit();
		});

		$("#productType").change(function() {
			$(".type_section").hide();
			$(".type_section .form-control").removeAttr("required");

			currentSection = $("#" + $(this).val().toLowerCase() + "_section");
			currentSection.show();
			currentSection.find(".form-control").attr("required", true);
		});
	});
})(jQuery);